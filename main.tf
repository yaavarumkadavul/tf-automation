terraform {
  required_version = ">= 0.12.28"
}

provider "aws" {
  version = "~> 2.8"
  region  = var.region
}

terraform {
  backend "s3" {}
}

locals {
  task2_max_subnet_length = max(
    length(var.private_subnets),
    length(var.database_subnets)
  )
  task2_nat_gateway_count = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(data.aws_availability_zones.task2_availableaz.names) : local.task2_max_subnet_length
}

data "aws_kms_alias" "task2_ssm" {
  name = "alias/aws/ssm"
}

data "aws_kms_alias" "task2_ebs" {
  name = "alias/aws/ebs"
}

data "aws_kms_alias" "s3" {
  name = "alias/aws/s3"
}

data "aws_elb_service_account" "main" {}

data "aws_region" "task2_current" {}

data "aws_availability_zones" "task2_availableaz" {
  state = "available"
}

data "aws_ami" "task2_amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

data "aws_partition" "task2_current" {}

data "aws_caller_identity" "task2_current" {}


data "archive_file" "init" {
  type        = "zip"
  source_dir  = "${path.root}/ansible-config"
  output_path = "${path.root}/ansible.zip"
}

module "task2_databucket" {
  source                  = "./modules/s3"
  bucket_prefix           = "databucket"
  s3_bucket_force_destroy = true
  create                  = var.create
  resource_create         = var.task2
  kms_arn                 = data.aws_kms_alias.s3.arn
  tags                    = var.tags
  versioning              = true
  sse_algorithm           = "AES256"
  policy                  = <<EOF
{
  "Version": "2012-10-17",
  "Id": "MYDATABUCKETPOLICY",
  "Statement": [
    {
      "Sid": "ELBAllow",
      "Effect": "Allow",
      "Principal": {
        "AWS": ["${data.aws_elb_service_account.main.arn}"]
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.create && var.task2 && length(module.task2_databucket.bucket_id) > 0 ? element(concat(module.task2_databucket.bucket_id, list("")), 0) : "*"}/logs/AWSLogs/${data.aws_caller_identity.task2_current.account_id}/*"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "delivery.logs.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.create && var.task2 && length(module.task2_databucket.bucket_id) > 0 ? element(concat(module.task2_databucket.bucket_id, list("")), 0) : "*"}/logs/AWSLogs/${data.aws_caller_identity.task2_current.account_id}/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "delivery.logs.amazonaws.com"
      },
      "Action": "s3:GetBucketAcl",
      "Resource": "arn:aws:s3:::${var.create && var.task2 && length(module.task2_databucket.bucket_id) > 0 ? element(concat(module.task2_databucket.bucket_id, list("")), 0) : "*"}"
    }
  ]
}
EOF
}

module "task2_application_upload_archive" {
  source            = "./modules/file_upload_s3"
  create            = var.create
  resource_create   = var.task2
  bucket_name       = join(" ", module.task2_databucket.bucket_id)
  tags              = var.tags
  file_name         = "${path.root}/ansible.zip"
  encryption_method = "AES256"
  force_destroy     = true
}

module "task2_instance_role_iam_policy" {
  source          = "./modules/iam/policy"
  create          = var.create
  resource_create = var.task2
  name            = "task2-custom-policy-ec2-s3"
  path            = "/"
  description     = "My custom policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "kms:Decrypt",
                "kms:ListKeyPolicies",
                "kms:ListRetirableGrants",
                "kms:Encrypt",
                "kms:GenerateDataKey",
                "kms:GenerateDataKeyWithoutPlaintext",
                "kms:DescribeKey",
                "kms:Verify",
                "kms:GenerateDataKeyPairWithoutPlaintext",
                "kms:GenerateDataKeyPair",
                "kms:ListGrants"
            ],
            "Resource": [
                "arn:aws:s3:::aws-ssm-region/*",
                "arn:aws:s3:::aws-windows-downloads-region/*",
                "arn:aws:s3:::amazon-ssm-region/*",
                "arn:aws:s3:::amazon-ssm-packages-region/*",
                "arn:aws:s3:::region-birdwatcher-prod/*",
                "arn:aws:s3:::aws-ssm-distributor-file-region/*",
                "arn:aws:s3:::patch-baseline-snapshot-region/*",
                "arn:aws:kms:${data.aws_region.task2_current.name}:${data.aws_caller_identity.task2_current.account_id}:key/*"
            ]
        },
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "kms:ListKeys",
                "iam:PassRole",
                "s3:ListAllMyBuckets",
                "kms:ListAliases"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetEncryptionConfiguration",
                "s3:ListBucket",
                "s3:GetBucketLocation",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${var.create && var.task2 && length(module.task2_databucket.bucket_id) > 0 ? element(concat(module.task2_databucket.bucket_id, list("")), 0) : "*"}",
                "arn:aws:s3:::${var.create && var.task2 && length(module.task2_databucket.bucket_id) > 0 ? format("%s/%s", element(concat(module.task2_databucket.bucket_id, list("")), 0), "*") : "*"}"
            ]
        }
    ]
}
EOF
}

module "task2_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task2
  roles           = module.task2_sg_devops_instance_role.role_name
  policy_arn      = module.task2_instance_role_iam_policy.arn
}

module "task2_managed_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task2
  roles           = module.task2_sg_devops_instance_role.role_name
  policy_arn      = ["arn:${data.aws_partition.task2_current.partition}:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

module "task2_managed_secret_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task2
  roles           = module.task2_sg_devops_instance_role.role_name
  policy_arn      = ["arn:${data.aws_partition.task2_current.partition}:iam::aws:policy/SecretsManagerReadWrite"]
}

module "task2_vpc_devops" {
  source          = "./modules/network/vpc"
  create          = var.create
  resource_create = var.task2
  cidr            = var.cidr
  name            = var.name
  env             = var.env
  tags            = var.tags
}

module "task2_sub_public_devops" {
  source                  = "./modules/network/subnets"
  create                  = var.create
  resource_create         = var.task2
  public_subnets          = var.public_subnets
  azs                     = data.aws_availability_zones.task2_availableaz.names
  vpc_id                  = module.task2_vpc_devops.vpc_id
  one_nat_gateway_per_az  = var.one_nat_gateway_per_az
  single_nat_gateway      = var.single_nat_gateway
  map_public_ip_on_launch = false
  subnet_suffix           = "task2_sub_public_devops"
  tags                    = var.tags
  env                     = var.env
}

module "task2_igw" {
  source          = "./modules/network/igw"
  public_subnets  = var.public_subnets
  create          = var.create
  resource_create = var.task2
  vpc_id          = module.task2_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "igw")
}

module "task2_public_rt" {
  source          = "./modules/network/rt"
  public_subnets  = var.public_subnets
  create          = var.create
  resource_create = var.task2
  vpc_id          = module.task2_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "task2_publicrt")
}

module "task2_public_rt_association" {
  source          = "./modules/network/rtassoc"
  create          = var.create
  resource_create = var.task2
  subnet_id       = module.task2_sub_public_devops.public_subnet_id
  rt_id           = module.task2_public_rt.public_rt_id
}

module "task2_public_subnet_route" {
  source          = "./modules/network/route"
  create          = var.create
  resource_create = var.task2
  create_igw      = true
  igw_id          = module.task2_igw.igw_id
  subnet_id       = module.task2_sub_public_devops.public_subnet_id
  rt_id           = module.task2_public_rt.public_rt_id
}

module "task2_nat_gw_eip" {
  source          = "./modules/network/eip"
  create          = var.create
  resource_create = var.task2
  eip_count       = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(data.aws_availability_zones.task2_availableaz.names) : local.task2_max_subnet_length
  name            = "task2_ngw-eip"
  tags            = var.tags
}

module "task2_private_subnet_nat_gateway" {
  source          = "./modules/network/natgw"
  create          = var.create
  resource_create = var.task2
  create_ngw      = var.one_nat_gateway_per_az || var.single_nat_gateway ? true : false
  gateway_count   = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(data.aws_availability_zones.task2_availableaz.names) : local.task2_max_subnet_length
  subnet_id       = module.task2_sub_public_devops.public_subnet_id
  allocation_id   = module.task2_nat_gw_eip.eip_id
  name            = "devops-ngw"
  tags            = var.tags
}

module "task2_sub_private_devops" {
  source          = "./modules/network/subnets"
  create          = var.create
  resource_create = var.task2
  private_subnets = var.private_subnets
  azs             = data.aws_availability_zones.task2_availableaz.names
  vpc_id          = module.task2_vpc_devops.vpc_id
  subnet_suffix   = "task2_sub_private_devops"
  tags            = var.tags
  env             = var.env
}

module "task2_private_rt" {
  source          = "./modules/network/rt"
  private_subnets = var.private_subnets
  gateway_count   = local.task2_nat_gateway_count
  create          = var.create
  resource_create = var.task2
  azs             = data.aws_availability_zones.task2_availableaz.names
  vpc_id          = module.task2_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "task2_privatert")
}

module "task2_private_rt_association" {
  source          = "./modules/network/rtassoc"
  create          = var.create
  resource_create = var.task2
  subnet_id       = module.task2_sub_private_devops.private_subnet_id
  rt_id           = module.task2_private_rt.private_rt_id
}

module "task2_private_subnet_route" {
  source          = "./modules/network/route"
  create          = var.create
  resource_create = var.task2
  create_ngw      = var.one_nat_gateway_per_az || var.single_nat_gateway ? true : false
  nat_gw_id       = module.task2_private_subnet_nat_gateway.nat_id
  subnet_id       = module.task2_sub_private_devops.private_subnet_id
  rt_id           = module.task2_private_rt.private_rt_id
}

module "task2_sg_devops" {
  source                 = "./modules/network/securitygroup"
  create                 = var.create
  resource_create        = var.task2
  vpc_id                 = module.task2_vpc_devops.vpc_id
  tags                   = var.tags
  env                    = var.env
  name                   = "task2_sg-devops"
  description            = "SG devops group-task2"
  revoke_rules_on_delete = true
}

module "task2_sg_devops_ingress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task2
  sg_id           = module.task2_sg_devops.sg_id
  type            = "ingress"
  ingress_rules   = var.public_ingress_rules
  cidr_blocks     = [var.cidr]
}

module "task2_sg_devops_egress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task2
  sg_id           = module.task2_sg_devops.sg_id
  ingress_rules   = ["all-tcp", "all-udp"]
  cidr_blocks     = ["0.0.0.0/0"]
}

module "task2_sg_devops_keygen" {
  source          = "./modules/keygen"
  create          = var.create
  resource_create = var.task2
}

module "task2_sg_devops_keypair" {
  source          = "./modules/keypair"
  create          = var.create
  resource_create = var.task2
  key_name        = format("%s-%s", var.env, "task2_keypair")
  public_key      = module.task2_sg_devops_keygen.key_public_openssh
}

module "task2_sg_devops_ssm" {
  source          = "./modules/ssm"
  create          = var.create
  resource_create = var.task2
  name            = format("/%s-%s/%s/%s", "task2_vpc", var.env, "keypair", "private")
  type            = "SecureString"
  value           = module.task2_sg_devops_keygen.key_private_pem
  key_id          = data.aws_kms_alias.task2_ssm.arn
  description     = "task2_private key"
  overwrite       = true
  tags            = var.tags
}

module "task2_sg_devops_instance_role" {
  source          = "./modules/iam/instance_role"
  create          = var.create
  resource_create = var.task2
  name            = "task2_ec2-role"
  tags            = var.tags
}

module "task2_sg_devops_instance_profile" {
  source          = "./modules/iam/instance_profile"
  create          = var.create
  resource_create = var.task2
  name            = "task2_ec2_devops-role"
  role            = module.task2_sg_devops_instance_role.role_name
}

data "template_file" "init" {
  count    = var.create && var.task2 ? 1 : 0
  template = "${file("${path.root}/scripts/task.sh.tpl")}"
  vars = {
    bucket_name = format("%s/%s", element(concat(module.task2_databucket.bucket_id, list("")), 0), "ansible.zip")
  }
}

module "task2_asg_launchconfig" {
  # Configure AWS LaunchConfig
  source                 = "./modules/AutoScaling/LaunchConfiguration"
  create                 = var.create
  resource_create        = var.task2
  ami                    = data.aws_ami.task2_amazon-linux-2.id
  name                   = "task2_asg"
  instance_type          = var.instance_type
  iam_instance_profile   = join("", module.task2_sg_devops_instance_profile.instance_profile_name)
  key_name               = module.task2_sg_devops_keypair.keypair_name
  user_data              = try(data.template_file.init[0].rendered, null)
  vpc_security_group_ids = module.task2_sg_devops.sg_id

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]

  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp2"
      volume_size = 5
      encrypted   = true
      kms_key_id  = data.aws_kms_alias.task2_ebs.arn
    }
  ]
}

module "task2_elb_sg" {
  source                 = "./modules/network/securitygroup"
  create                 = var.create
  resource_create        = var.task2
  vpc_id                 = module.task2_vpc_devops.vpc_id
  tags                   = var.tags
  env                    = var.env
  name                   = "task2_elb_sg"
  description            = "SG devops group-task2 elb"
  revoke_rules_on_delete = true
}

module "task2_elb_sg_ingress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task2
  sg_id           = module.task2_elb_sg.sg_id
  type            = "ingress"
  ingress_rules   = ["http-80-tcp"]
  cidr_blocks     = ["0.0.0.0/0"]
}

module "task2_elb_sg_egress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task2
  sg_id           = module.task2_elb_sg.sg_id
  ingress_rules   = ["all-tcp", "all-udp"]
  cidr_blocks     = ["0.0.0.0/0"]
}

module "task2_elb" {
  # Configure AWS Loadbalancer
  source          = "./modules/AutoScaling/ELB"
  create          = var.create
  resource_create = var.task2
  name            = "tas2-elb-front"
  subnets         = module.task2_sub_public_devops.public_subnet_id
  security_groups = module.task2_elb_sg.sg_id
  internal        = false

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    },
  ]

  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  access_logs = {
    bucket        = element(concat(module.task2_databucket.bucket_id, list("")), 0)
    bucket_prefix = "logs"
  }

  tags = var.tags
}

module "asg" {
  source               = "./modules/AutoScaling/ASG"
  create               = var.create
  resource_create      = var.task2
  name                 = "task2-devops-asg"
  launch_configuration = module.task2_asg_launchconfig.launchconfigurationname
  vpc_zone_identifier  = module.task2_sub_private_devops.private_subnet_id
  max_size             = 3
  min_size             = 2
  desired_capacity     = 2
  load_balancers       = module.task2_elb.elb_name
  tags_as_map          = var.asg_tags
  health_check_type    = "EC2"
  #force_delete = true
}



################### Green resources ##################################
module "task2_asg_launchconfig_green" {
  # Configure AWS LaunchConfig
  source                 = "./modules/AutoScaling/LaunchConfiguration"
  create                 = var.create
  resource_create        = var.task2 && var.green
  ami                    = data.aws_ami.task2_amazon-linux-2.id
  name                   = "task2_asg-green"
  instance_type          = var.instance_type
  iam_instance_profile   = join("", module.task2_sg_devops_instance_profile.instance_profile_name)
  key_name               = module.task2_sg_devops_keypair.keypair_name
  user_data              = try(data.template_file.init[0].rendered, null)
  vpc_security_group_ids = module.task2_sg_devops.sg_id

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]

  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp2"
      volume_size = 5
      encrypted   = true
      kms_key_id  = data.aws_kms_alias.task2_ebs.arn
    }
  ]
}

module "task2_elb_green" {
  # Configure AWS Loadbalancer
  source          = "./modules/AutoScaling/ELB"
  create          = var.create
  resource_create = var.task2 && var.green
  name            = "tas2-elb-front-green"
  subnets         = module.task2_sub_public_devops.public_subnet_id
  security_groups = module.task2_elb_sg.sg_id
  internal        = false

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    },
  ]

  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  access_logs = {
    bucket        = element(concat(module.task2_databucket.bucket_id, list("")), 0)
    bucket_prefix = "logs"
  }

  tags = var.tags
}

module "asg_green" {
  source               = "./modules/AutoScaling/ASG"
  create               = var.create
  resource_create      = var.task2 && var.green
  name                 = "task2-devops-asg-green"
  launch_configuration = module.task2_asg_launchconfig_green.launchconfigurationname
  vpc_zone_identifier  = module.task2_sub_private_devops.private_subnet_id
  max_size             = 3
  min_size             = 2
  desired_capacity     = 2
  load_balancers       = module.task2_elb_green.elb_name
  tags_as_map          = var.asg_tags
  health_check_type    = "EC2"
  #force_delete = true
}


// ##############################################

// #---------------------------------------------------------------------------------------------------
// # Output Block
// #---------------------------------------------------------------------------------------------------

output "etag" {
  value = module.task2_application_upload_archive.etag
}

output "object_version" {
  value = module.task2_application_upload_archive.object_version
}

output "task2_account_id" {
  value = data.aws_caller_identity.task2_current.account_id
}

output "task2_caller_arn" {
  value = data.aws_caller_identity.task2_current.arn
}

output "task2_caller_user" {
  value = data.aws_caller_identity.task2_current.user_id
}

output "task2_availabeaznames" {
  value = data.aws_availability_zones.task2_availableaz.names
}

output "task2_availabeazid" {
  value = data.aws_availability_zones.task2_availableaz.zone_ids
}

output "task2_regionname" {
  value = data.aws_region.task2_current.name
}

output "task2_vpc_id" {
  value = module.task2_vpc_devops.vpc_id
}

output "task2_vpc_arn" {
  value = module.task2_vpc_devops.vpc_arn
}

output "task2_vpc_cidr_block" {
  value = module.task2_vpc_devops.vpc_cidr_block
}

output "task2_public_subnet_id" {
  value = module.task2_sub_public_devops.public_subnet_id
}

output "task2_public_subnet_arn" {
  value = module.task2_sub_public_devops.public_subnet_arn
}

output "task2_private_subnet_id" {
  value = module.task2_sub_private_devops.private_subnet_id
}

output "task2_private_subnet_arn" {
  value = module.task2_sub_private_devops.private_subnet_arn
}

output "task2_sg_devops_sgname" {
  value = module.task2_sg_devops.sg_name
}

output "task2_sg_devops_sgarn" {
  value = module.task2_sg_devops.sg_arn
}

output "task2_sg_devops_sgid" {
  value = module.task2_sg_devops.sg_id
}

output "task2_sg_devops_publicrule_ingress_id" {
  value = module.task2_sg_devops_ingress.sg_rule_id
}

output "task2_sg_devops_keypair_ssmarn" {
  value = module.task2_sg_devops_ssm.ssm_arn
}







