# terraform-project-gitlab

Steps to recreate deployment:

1. Login to gitlab.com
2. Setup work dir in local environment
3. Create New Project and navigate to Setting and `Expand` Variables and start creating the following variables for our      pipeline.
    a. AWS_ACCESS_KEY_ID --> aws access key (masked)
    b. AWS_DEFAULT_REGION --> aws region
    c. AWS_DYNAMODB_STATE --> dynamodb table name, (gitlab-ci-demo-backend-db)
    d. AWS_KMS_KEY_ID --> aws/s3 kms (masked)
    e. AWS_REMOTESTATE_KEY --> mediawiki
    f. AWS_SECRET_ACCESS_KEY --> aws secret key (masked)
    g. AWS_STATE_BUCKET --> state bucket name (gitlab-ci-demo-backend-buk)
4. Enable `Auto Devops` and map your `.gitlab-ci.yml` under settings for enabling trigger.
5. Create S3 bucket with the below policy
{
    "Version": "2012-10-17",
    "Id": "RequireEncryption",
    "Statement": [
        {
            "Sid": "RequireEncryptedTransport",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::gitlab-ci-demo-backend-buk/*",
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "RequireEncryptedStorage",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::gitlab-ci-demo-backend-buk/*",
            "Condition": {
                "StringNotEquals": {
                    "s3:x-amz-server-side-encryption": "AES256"
                }
            }
        }
    ]
}
6. Create dynamo db as per the link: https://www.terraform.io/docs/backends/types/s3.html
    create table with `LockID` of type string as Primary key
7. Edit master.tfvars and change the value from `false` to `true`
8. Edit task.tfvars and change the value `task2` from `false` to `true`
9. Commit and Push the changes to repo and you can view CI under CI/CD --> pipelines
10. The step `TF_APPLY` is set to manual to control the deployment.
11. Once approved the setup is complete.